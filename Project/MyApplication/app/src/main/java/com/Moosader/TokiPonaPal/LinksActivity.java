package com.Moosader.TokiPonaPal;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.Moosader.TokiPonaPal.R;

public class LinksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_links);
    }

    public void closeActivity(View view) {
        finish();
    }

    public void gotoTokiPonaHomepage(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.tokipona.org/"));
        startActivity(browserIntent);
    }

    public void gotoChuffList(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/spreadsheets/d/12gDr-zsUuwwCWPme9DlAE0JWuFDAFrqh3_IA257ff1U/edit#gid=0"));
        startActivity(browserIntent);
    }

    public void gotoTokiPonaNet(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://tokipona.net/tp/Default.aspx"));
        startActivity(browserIntent);
    }

    public void gotoFacebook(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/groups/sitelen/"));
        startActivity(browserIntent);
    }

    public void gotoGooglePlus(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/communities/100025282683642339711"));
        startActivity(browserIntent);
    }

    public void gotoReddit(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.reddit.com/r/tokipona"));
        startActivity(browserIntent);
    }

    public void gotoAyadan(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://ayadan.moosader.com/toki-pona/"));
        startActivity(browserIntent);
    }
}

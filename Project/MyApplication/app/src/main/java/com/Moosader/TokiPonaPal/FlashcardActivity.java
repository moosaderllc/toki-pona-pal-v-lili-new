package com.Moosader.TokiPonaPal;

import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import com.Moosader.TokiPonaPal.R;

public class FlashcardActivity extends AppCompatActivity {

    private class Flashcard
    {
        public String tokipona;
        public String[] englishTerms;
    };

    ArrayList<Flashcard> flashcards = new ArrayList<Flashcard>();
    ArrayList<Flashcard> currentDeck;

    String flashcardSet;

    int totalCorrect;
    ArrayList<String> problemWordsA = new ArrayList<String>();
    ArrayList<String> problemWordsB = new ArrayList<String>();

    String currentQuestion = new String();
    String rightAnswer = new String();
    int currentQuestionIndex;

    Random randomer = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flashcard);
        loadFlashcards();
        gotoFlashcardChooser();
    }

    public void closeActivity(View view) {
        finish();
    }

    public void startEnglishCards(View view)
    {
        flashcardSet = "english";
        gotoFlashcardQuizzer();
        setupFlashcards();
    }

    public void startTokiponaCards(View view)
    {
        flashcardSet = "tokipona";
        gotoFlashcardQuizzer();
        setupFlashcards();
    }

    public void gotoFlashcardChooser()
    {
        LinearLayout layoutOptions = (LinearLayout)findViewById(R.id.ChooseCards);
        LinearLayout layoutQuizzer = (LinearLayout)findViewById(R.id.Quizzer);

        layoutOptions.setVisibility(View.VISIBLE);
        layoutQuizzer.setVisibility(View.GONE);
    }

    public void gotoFlashcardQuizzer()
    {
        LinearLayout layoutOptions = (LinearLayout)findViewById(R.id.ChooseCards);
        LinearLayout layoutQuizzer = (LinearLayout)findViewById(R.id.Quizzer);

        layoutOptions.setVisibility(View.GONE);
        layoutQuizzer.setVisibility(View.VISIBLE);
    }

    public void setupFlashcards()
    {
        totalCorrect = 0;

        currentDeck = new ArrayList<Flashcard>(flashcards);

        updateStatus();
        newQuestion();
    }

    public void updateStatus()
    {
        String strRemaining = getResources().getString(R.string.remaining_questions);
        TextView txtRemaining = (TextView)findViewById(R.id.txtRemaining);
        txtRemaining.setText(strRemaining + " " + currentDeck.size());

        String strCorrect = getResources().getString(R.string.correct);
        TextView txtCorrect = (TextView)findViewById(R.id.txtCorrect);
        txtCorrect.setText(strCorrect + " " + totalCorrect);
    }

    public void gotoNextQuestion(View view)
    {
        if ( currentDeck.size() > 0 ) {
            newQuestion();
        }
    }

    public void newQuestion()
    {
        if ( currentDeck == null ) {
            return;
        }

        // Display stuff
        Button[] buttons = new Button[3];
        buttons[0] = (Button)findViewById(R.id.btnAnswer1);
        buttons[1] = (Button)findViewById(R.id.btnAnswer2);
        buttons[2] = (Button)findViewById(R.id.btnAnswer3);

        for ( int i = 0; i < 3; i++ )
        {
            buttons[i].setVisibility(View.VISIBLE);
        }

        TextView txtQuestion = (TextView)findViewById(R.id.txtQuestion);
        txtQuestion.setVisibility(View.VISIBLE);

        TextView txtResult = (TextView)findViewById(R.id.txtResult);
        txtResult.setVisibility(View.GONE);

        Button btnNext = (Button)findViewById(R.id.btnNext);
        btnNext.setVisibility(View.GONE);

        ScrollView problemView = (ScrollView)findViewById(R.id.ProblemView);
        problemView.setVisibility(View.GONE);


        // Get random entry
        int index = randomer.nextInt(currentDeck.size());
        currentQuestionIndex = index;
        int eng = randomer.nextInt(3);

        // Set question
        if ( flashcardSet.equals("english"))
        {
            // Display
            currentQuestion = currentDeck.get(index).englishTerms[eng];
            rightAnswer = currentDeck.get(index).tokipona;
        }
        else if (flashcardSet.equals("tokipona")) {
            // Display
            currentQuestion = currentDeck.get(index).tokipona;
            rightAnswer = currentDeck.get(index).englishTerms[eng];
        }

        txtQuestion.setText(currentQuestion);

        // Setup answers
        for ( int i = 0; i < 3; i++ )
        {
            int ansIndex = randomer.nextInt(currentDeck.size());

            if (flashcardSet.equals("english")) {
                buttons[i].setText(currentDeck.get(ansIndex).tokipona);
            }
            else if (flashcardSet.equals("tokipona"))
            {
                buttons[i].setText(currentDeck.get(ansIndex).englishTerms[eng]);
            }
        }

        int randButton = randomer.nextInt(3);
        if (flashcardSet.equals("english")) {
            buttons[randButton].setText(rightAnswer);
        }
        else if (flashcardSet.equals("tokipona"))
        {
            buttons[randButton].setText(rightAnswer);
        }

        txtResult.setVisibility(View.INVISIBLE);
    }

    public void checkAnswer(int answer)
    {
        Button[] buttons = new Button[3];
        buttons[0] = (Button)findViewById(R.id.btnAnswer1);
        buttons[1] = (Button)findViewById(R.id.btnAnswer2);
        buttons[2] = (Button)findViewById(R.id.btnAnswer3);

        TextView txtResult = (TextView)findViewById(R.id.txtResult);
        Button btnNext = (Button)findViewById(R.id.btnNext);

        ScrollView problemView = (ScrollView)findViewById(R.id.ProblemView);
        problemView.setVisibility(View.VISIBLE);

        // Adjust display
        for ( int i = 0; i < 3; i++ )
        {
            buttons[i].setVisibility(View.GONE);
        }
        btnNext.setVisibility(View.VISIBLE);
        txtResult.setVisibility(View.VISIBLE);

        if (buttons[answer].getText() == rightAnswer)
        {
            totalCorrect++;
            String strCorrect = getResources().getString(R.string.correctanswer);
            txtResult.setText(strCorrect);
            txtResult.setTextColor(Color.BLUE);
        }
        else
        {
            String strIncorrect = getResources().getString(R.string.incorrectanswer);
            txtResult.setText(strIncorrect);
            problemWordsA.add(currentQuestion);
            problemWordsB.add(rightAnswer);
            txtResult.setTextColor(Color.RED);
        }

        // Remove this current question
        currentDeck.remove(currentQuestionIndex);


        if ( currentDeck.size() <= 0 ) {
            String strNoneLeft = getResources().getString(R.string.no_questions_remaining);
            TextView txtQuestion = (TextView)findViewById(R.id.txtQuestion);
            txtQuestion.setText(strNoneLeft);
            btnNext.setVisibility(View.GONE);
        }

        updateStatus();
        displayProblemWords();
    }

    public void displayProblemWords()
    {
        String strProblemWords = getResources().getString(R.string.problem_words);
        TextView txtProblemWords = (TextView)findViewById(R.id.txtProblemWords);
        String text = strProblemWords + System.getProperty("line.separator") + System.getProperty("line.separator");

        for ( int i = 0; i < problemWordsA.size(); i++ )
        {
            text += "• " + problemWordsA.get(i) + " (" + problemWordsB.get(i) + ")" + System.getProperty("line.separator");
        }

        txtProblemWords.setText(text);
    }

    public void choseAnswer1(View view)
    {
        checkAnswer(0);
    }

    public void choseAnswer2(View view)
    {
        checkAnswer(1);
    }

    public void choseAnswer3(View view)
    {
        checkAnswer(2);
    }

    public void loadFlashcards() {
        Resources res = getResources();
        InputStream inStream = res.openRawResource(R.raw.flashcards);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));

        String splitBy = "~";

        try
        {
            String line = reader.readLine(); // Skip the header
            while ( line != null )
            {
                line = reader.readLine();
                if ( line != null )
                {
                    String[] info = line.split(splitBy);

                    Flashcard newFlashcard = new Flashcard();
                    newFlashcard.englishTerms = new String[3];

                    newFlashcard.tokipona = info[3];
                    newFlashcard.englishTerms[0] = info[0];
                    newFlashcard.englishTerms[1] = info[1];
                    newFlashcard.englishTerms[2] = info[2];
                    flashcards.add(newFlashcard);
                }
            }
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
    }
}

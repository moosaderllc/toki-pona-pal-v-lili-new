package com.Moosader.TokiPonaPal;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class BookshelfActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookshelf);

        TextView txtAuthor1 = (TextView)findViewById(R.id.txtAuthor1);
        txtAuthor1.setText( getResources().getString(R.string.written_by) + " Rachel Jane Morris" );

        TextView txtAuthor2 = (TextView)findViewById(R.id.txtAuthor2);
        txtAuthor2.setText( getResources().getString(R.string.written_by) + " Rachel Jane Morris" );

        TextView txtAuthor3 = (TextView)findViewById(R.id.txtAuthor3);
        txtAuthor3.setText( getResources().getString(R.string.written_by) + " Rachel Jane Morris" );


        LinearLayout layoutChooser = (LinearLayout)findViewById(R.id.BookChooser);
        LinearLayout layoutTextReader = (LinearLayout)findViewById(R.id.TextReader);
        LinearLayout layoutComicReader = (LinearLayout)findViewById(R.id.ComicReader);

        layoutChooser.setVisibility(View.VISIBLE);
        layoutTextReader.setVisibility(View.GONE);
        layoutComicReader.setVisibility(View.GONE);
    }

    public void closeActivity(View view) {
        finish();
    }

    public void gotoBookshelf(View view) {
        LinearLayout layoutChooser = (LinearLayout)findViewById(R.id.BookChooser);
        LinearLayout layoutTextReader = (LinearLayout)findViewById(R.id.TextReader);
        LinearLayout layoutComicReader = (LinearLayout)findViewById(R.id.ComicReader);

        layoutChooser.setVisibility(View.VISIBLE);
        layoutTextReader.setVisibility(View.GONE);
        layoutComicReader.setVisibility(View.GONE);
    }

    public void openStory1(View view) {
        //openTextStory(R.raw.story_jan_lili_en_soweli);//"story_jan_lili_en_soweli");
        openComicStory(R.drawable.comic_lesson2);
    }

    public void openStory2(View view) {
        openComicStory(R.drawable.comic_lesson3);
    }

    public void openStory3(View view) {
        openComicStory(R.drawable.comic_lesson5);
    }

    public void openTextStory(int storyname) {
        // Open story document and load it into the txtStory item
        LinearLayout layoutChooser = (LinearLayout)findViewById(R.id.BookChooser);
        LinearLayout layoutTextReader = (LinearLayout)findViewById(R.id.TextReader);
        LinearLayout layoutComicReader = (LinearLayout)findViewById(R.id.ComicReader);

        layoutChooser.setVisibility(View.GONE);
        layoutTextReader.setVisibility(View.VISIBLE);
        layoutComicReader.setVisibility(View.GONE);


        Resources res = getResources();
        InputStream inStream = res.openRawResource(storyname);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));

        String story = "";

        try
        {
            String line = "bob";
            while ( line != null )
            {
                line = reader.readLine();
                if ( line != null )
                {
                    story += line + System.getProperty("line.separator");
                }
            }
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }

        TextView txtStory = (TextView)findViewById(R.id.txtStory);
        txtStory.setText(story);
    }

    public void openComicStory(int storyname) {
        // Open story document and load it into the txtStory item
        LinearLayout layoutChooser = (LinearLayout)findViewById(R.id.BookChooser);
        LinearLayout layoutTextReader = (LinearLayout)findViewById(R.id.TextReader);
        LinearLayout layoutComicReader = (LinearLayout)findViewById(R.id.ComicReader);

        layoutChooser.setVisibility(View.GONE);
        layoutTextReader.setVisibility(View.GONE);
        layoutComicReader.setVisibility(View.VISIBLE);


        ImageView comicDisplay = (ImageView)findViewById(R.id.imgComic);
        comicDisplay.setImageResource(storyname);
    }
}

# Toki Pona Pal

## Ideas for new features

### Bookshelf 
Add a comic reader and story reader for stories

### Phrase Builder
Build a sentence a word at a time; a drop-down with subjects, objects, etc.

### Decoder
Input toki pona text in a textbox, and it will give you a breakdown of every word in the sentence.

### Typer
Type sentences with the Toki Pona glyphs

### Virtual Pet
Simple game using Toki Pona vocabulary with a tomogatchi-like pet

### Number Game
Build larger numbers with the smaller bits: 1, 2, 5, 10, etc.

### Comic Builder
Allow people to move sprites around, word balloons,
and overlay Toki Pona text to create new Toki Pona content.
Save/Export to PNG or something, share on social mediaz.

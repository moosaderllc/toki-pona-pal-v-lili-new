package com.Moosader.TokiPonaPal;

import android.content.res.Resources;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.Moosader.TokiPonaPal.R;

public class DictionaryActivity extends AppCompatActivity {

    private class DictionaryEntry
    {
        public String tokipona;
        public String wordType;
        public String description;
        public char firstLetter;
    };

    ArrayList<DictionaryEntry> dictionaryEntries = new ArrayList<DictionaryEntry>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);

        loadDictionary();
        dictionary_ViewAll();

        EditText searchBox = (EditText)findViewById(R.id.txtFilter);
        if (searchBox != null) {
            searchBox.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    dictionary_search(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }

    public void closeActivity(View view) {
        finish();
    }

    public void loadDictionary() {
        Resources res = getResources();
        InputStream inStream = res.openRawResource(R.raw.dictionary);
        BufferedReader reader = new BufferedReader(new InputStreamReader(inStream));

        String splitBy = "~";

        try
        {
            String line = reader.readLine(); // Skip the header
            while ( line != null )
            {
                line = reader.readLine();
                if ( line != null )
                {
                    String[] info = line.split(splitBy);

                    DictionaryEntry newEntry = new DictionaryEntry();
                    newEntry.tokipona = info[0];
                    newEntry.wordType = info[1];
                    newEntry.description = info[2];
                    newEntry.firstLetter = info[0].charAt(0);
                    dictionaryEntries.add(newEntry);
                }
            }
        }
        catch( IOException e )
        {
            e.printStackTrace();
        }
    }

    public void dictionary_ViewAll() {
        String dictionaryContent = new String("");

        for (int i = 0; i < dictionaryEntries.size(); i++)
        {
            dictionaryContent += dictionaryEntries.get(i).tokipona.toLowerCase() + "     ("
                    + dictionaryEntries.get(i).wordType.toUpperCase() + ")" + System.getProperty("line.separator")
                    + dictionaryEntries.get(i).description + System.getProperty("line.separator") + System.getProperty("line.separator");
        }

        TextView dictOutput = (TextView)findViewById(R.id.txtDictionary);
        dictOutput.setText( dictionaryContent );
    }

    public void dictionary_gotoEntry(char letter)
    {
        String dictionaryContent = new String("");

        for (int i = 0; i < dictionaryEntries.size(); i++)
        {
            if ( dictionaryEntries.get(i).firstLetter == letter ) {
                dictionaryContent += dictionaryEntries.get(i).tokipona.toLowerCase() + "     ("
                        + dictionaryEntries.get(i).wordType.toUpperCase() + ")" + System.getProperty("line.separator")
                        + dictionaryEntries.get(i).description + System.getProperty("line.separator") + System.getProperty("line.separator");
            }
        }

        TextView dictOutput = (TextView)findViewById(R.id.txtDictionary);
        dictOutput.setText( dictionaryContent );
    }

    public void dictionary_search(String term)
    {
        String dictionaryContent = new String("");

        for (int i = 0; i < dictionaryEntries.size(); i++)
        {
            if ( dictionaryEntries.get(i).tokipona.contains(term)
                    || dictionaryEntries.get(i).description.contains(term)
                    || dictionaryEntries.get(i).wordType.contains(term)
                    )
            {
                dictionaryContent += dictionaryEntries.get(i).tokipona.toLowerCase() + "     ("
                        + dictionaryEntries.get(i).wordType.toUpperCase() + ")" + System.getProperty("line.separator")
                        + dictionaryEntries.get(i).description + System.getProperty("line.separator") + System.getProperty("line.separator");
            }
        }

        TextView dictOutput = (TextView)findViewById(R.id.txtDictionary);
        dictOutput.setText( dictionaryContent );
    }

    public void btnViewAll(View view){
        dictionary_ViewAll();
    }

    public void btnViewA(View view){
        dictionary_gotoEntry('a');
    }

    public void btnViewE(View view){
        dictionary_gotoEntry('e');
    }

    public void btnViewI(View view){
        dictionary_gotoEntry('i');
    }

    public void btnViewJ(View view){
        dictionary_gotoEntry('j');
    }

    public void btnViewK(View view){
        dictionary_gotoEntry('k');
    }

    public void btnViewL(View view){
        dictionary_gotoEntry('l');
    }

    public void btnViewM(View view){
        dictionary_gotoEntry('m');
    }

    public void btnViewN(View view){
        dictionary_gotoEntry('n');
    }

    public void btnViewO(View view){
        dictionary_gotoEntry('o');
    }

    public void btnViewP(View view){
        dictionary_gotoEntry('p');
    }

    public void btnViewS(View view){
        dictionary_gotoEntry('s');
    }

    public void btnViewT(View view){
        dictionary_gotoEntry('t');
    }

    public void btnViewU(View view){
        dictionary_gotoEntry('u');
    }

    public void btnViewW(View view){
        dictionary_gotoEntry('w');
    }
}

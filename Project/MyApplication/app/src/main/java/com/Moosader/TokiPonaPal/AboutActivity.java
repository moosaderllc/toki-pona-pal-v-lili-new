package com.Moosader.TokiPonaPal;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.Moosader.TokiPonaPal.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }

    public void closeActivity(View view) {
        finish();
    }

    public void gotoMoosaderHomepage(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.moosader.com/"));
        startActivity(browserIntent);
    }

    public void gotoTokiPonaHomepage(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.tokipona.org/"));
        startActivity(browserIntent);
    }

    public void gotoAmazonStore(View view) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.amazon.com/Toki-Pona-Language-Sonja-Lang-ebook/dp/B012M1RLXS"));
        startActivity(browserIntent);
    }
}
